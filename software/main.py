# -*- coding: utf-8 -*-			
from Tkinter import *			
import tkMessageBox  			
from PIL import Image, ImageTk	
from matplotlib import pyplot	
import math						
import scipy					
import scipy.optimize			
import scipy.linalg as la 
from numpy import *		
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.pylab as p

'''
Python 2.7.12 x64
'''

root = Tk() 									#creamos la ventana
root.title('Métodos numéricos') 				#nombre de la ventana
root.iconbitmap('img\icon.ico')                 #icono del programa
root.geometry('930x520') 			            #tamaño de ventana 930x520
root.config(bg='#272822') 			            #color de fondo negro en la ventana root

def fondo():
	auxLabel = Label(root, bg="#272822", width=930, height=520).grid(row=0, column=0, sticky=W)

def posFalsaAct():
	fondo()
	L1 = Label(root, bg='#272822', font=("Helvetica", 16), fg='#5591C7', justify=LEFT, text="Posición falsa").place(x=10, y=10)
	L2 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="x0 = ").place(x=30, y=50)
	x0String = StringVar()
	x0Entry = Entry(root, bd=1, width=20, textvariable=x0String).place(x=70, y=50)
	L2 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="x1 = ").place(x=30, y=80)
	x1String = StringVar()
	x1Entry = Entry(root, bd=1, width=20, textvariable=x1String).place(x=70, y=80)

	def f(x):
		return math.cos(x) - x**3

	def rootF(a, b):
		return b - (f(b)*((a-b)/(f(a)-f(b))))

	def regF(a, b):
		itr = 0
		maxItr = 100
		while (itr < maxItr):
			r = rootF(a,b)
			if (r < 0):
				b = r
			else:
				a = r
			itr = itr + 1
		return r

	def calcularF():
		try:
			x0 = float(x0String.get())
			x1 = float(x1String.get())

			rootVal = regF(x0, x1)

			L0 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, width=20, height=10, text="Raíz = " + str(rootVal)).place(x=30, y=200)
		
		except ValueError:
			tkMessageBox.showerror("Error", "Consulte el apartado de Ayuda para más información sobre este error.")

	calcBtn = Button(root, text="Calcular", command=calcularF).place(x=70, y=110)

def encRaizAct():
	fondo()
	L1 = Label(root, bg='#272822', font=("Helvetica", 16), fg='#5591C7', justify=LEFT, text="Encontrar la raíz").place(x=10, y=10)
	L2 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="a = ").place(x=30, y=50)
	aString = StringVar()
	aEntry = Entry(root, bd=1, width=20, textvariable=aString).place(x=70, y=50)
	L2 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="b = ").place(x=30, y=80)
	bString = StringVar()
	bEntry = Entry(root, bd=1, width=20, textvariable=bString).place(x=70, y=80)
	L3 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="c = ").place(x=30, y=110)
	cString = StringVar()
	cEntry = Entry(root, bd=1, width=20, textvariable=cString).place(x=70, y=110)
	
	img = Image.open('img\output.png')
	img = img.resize((600, 400), Image.ANTIALIAS)
	render = ImageTk.PhotoImage(img)
	charac = Label(root, image=render, bg='#222B36', bd=0)
	charac.image = render
	charac.place(x=310, y=50)

	def f1(x):
    		return a*(x**2) + b*x - c

	def calcularF():
		try:
			global a, b, c
			a=float(aString.get())
			b=float(bString.get())
			c=float(cString.get())

			disc=b*b-4*a*c
			if(a!=0):
				if(disc<0):
			  		tkMessageBox.showerror("Error", "Contiene raices imaginarias")
			 	else:
					x1=(-b+(math.sqrt(disc)))/(2*a)
					x2=(-b-(math.sqrt(disc)))/(2*a)
					L0 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, width=20, height=10, text="x1 = "+str(x1)+"\nx2 = "+str(x2)).place(x=30, y=200)

					x = range(-10, 10)
					pyplot.plot(x, [f1(i) for i in x])
					pyplot.axhline(0, color="black")
					pyplot.axvline(0, color="black")
					pyplot.ylim(-100, 100)
					pyplot.xlim(-100, 100)
					pyplot.savefig("img\cuadratica.png")

					img = Image.open('img\cuadratica.png')
					img = img.resize((600, 400), Image.ANTIALIAS)
					render = ImageTk.PhotoImage(img)
					charac = Label(root, image=render, bg='#222B36', bd=0)
					charac.image = render
					charac.place(x=310, y=50)
					pyplot.clf()
			else:
				tkMessageBox.showerror("Error", "El coefiente cuadrático debe ser diferente de cero.\na ≠ 0")

		except ValueError:
			tkMessageBox.showerror("Error", "Consulte el apartado de Ayuda para más información sobre este error.")

	calcBtn = Button(root, text="Calcular y graficar", command=calcularF).place(x=70, y=150)

def apPolSimAct():
	fondo()
	L1 = Label(root, bg='#272822', font=("Helvetica", 16), fg='#5591C7', justify=LEFT, text="Aproximación polinomial simple (3er. grado)").place(x=10, y=10)
	
	L2 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="X³ = ").place(x=30, y=50)
	X3String = StringVar()
	x3Entry = Entry(root, bd=1, width=20, textvariable=X3String).place(x=100, y=50)
	L3 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="X² = ").place(x=30, y=80)
	X2String = StringVar()
	x2Entry = Entry(root, bd=1, width=20, textvariable=X2String).place(x=100, y=80)
	L4 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="X¹ = ").place(x=30, y=110)
	X1String = StringVar()
	x1Entry = Entry(root, bd=1, width=20, textvariable=X1String).place(x=100, y=110)
	L5 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="Const = ").place(x=30, y=140)
	CString = StringVar()
	CEntry = Entry(root, bd=1, width=20, textvariable=CString).place(x=100, y=140)

	img = Image.open('img\output.png')
	img = img.resize((600, 400), Image.ANTIALIAS)
	render = ImageTk.PhotoImage(img)
	charac = Label(root, image=render, bg='#222B36', bd=0)
	charac.image = render
	charac.place(x=310, y=50)

	def f(x):
    		return x3*(x**3) + x2*(x**2) + x1*x + x0

	def calcularF():
		try:
			global x3, x2, x1, x0
			x3 = float(X3String.get())
			x2 = float(X2String.get())
			x1 = float(X1String.get())
			x0 = float(CString.get())

			x = np.linspace(-10, 10)
			plt.plot(x, f(x));
			pyplot.axhline(0, color="black")
			pyplot.axvline(0, color="black")
			plt.ylim(-100, 100)
			plt.xlim(-100, 100)
			plt.savefig("img\polinomio.png")

			img = Image.open('img\polinomio.png')
			img = img.resize((600, 400), Image.ANTIALIAS)
			render = ImageTk.PhotoImage(img)
			charac = Label(root, image=render, bg='#222B36', bd=0)
			charac.image = render
			charac.place(x=310, y=50)
			plt.clf()
		except ValueError:
			tkMessageBox.showerror("Error", "Consulte el apartado de Ayuda para más información sobre este error.")

	calcBtn = Button(root, text="Calcular y graficar", command=calcularF).place(x=70, y=180)

def bisecAct():
	fondo()
	L1 = Label(root, bg='#272822', font=("Helvetica", 16), fg='#5591C7', justify=LEFT, text="Bisección").place(x=10, y=10)
	L2 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="Intervalo inicial").place(x=30, y=50)
	IniString = StringVar()
	aEntry = Entry(root, bd=1, width=20, textvariable=IniString).place(x=170, y=50)
	L3 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="Intervalo final").place(x=30, y=80)
	FinString = StringVar()
	bEntry = Entry(root, bd=1, width=20, textvariable=FinString).place(x=170, y=80)

	L4 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="X³ = ").place(x=30, y=130)
	X3String = StringVar()
	x3Entry = Entry(root, bd=1, width=20, textvariable=X3String).place(x=170, y=130)
	L5 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="X² = ").place(x=30, y=160)
	X2String = StringVar()
	x2Entry = Entry(root, bd=1, width=20, textvariable=X2String).place(x=170, y=160)
	L6 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="X¹ = ").place(x=30, y=190)
	X1String = StringVar()
	x1Entry = Entry(root, bd=1, width=20, textvariable=X1String).place(x=170, y=190)
	L7 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="Const = ").place(x=30, y=220)
	CString = StringVar()
	CEntry = Entry(root, bd=1, width=20, textvariable=CString).place(x=170, y=220)

	img = Image.open('img\output.png')
	img = img.resize((600, 400), Image.ANTIALIAS)
	render = ImageTk.PhotoImage(img)
	charac = Label(root, image=render, bg='#222B36', bd=0)
	charac.image = render
	charac.place(x=310, y=50)

	def f(x):
    		return x3*(x**3) + x2*(x**2) + x1*x + x0

	def calcularF():
		try:
			global x3, x2, x1, x0, ini, lim
			x3 = float(X3String.get())
			x2 = float(X2String.get())
			x1 = float(X1String.get())
			x0 = float(CString.get())
			ini = float(IniString.get())
			lim = float(FinString.get())

			scipy.optimize.bisect(f, lim, ini)
			raiz = repr(scipy.optimize.bisect(f, lim, ini))
			L0 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, width=30, height=5, text="Raíz = " + raiz).place(x=30, y=300)

			x = np.linspace(-10,10)
			plt.plot(x, f(x));
			pyplot.axhline(0, color="black")
			pyplot.axvline(0, color="black")
			plt.ylim(-100, 100)
			plt.xlim(-100, 100)
			plt.savefig("img\dbiseccion.png")

			img = Image.open("img\dbiseccion.png")
			img = img.resize((600, 400), Image.ANTIALIAS)
			render = ImageTk.PhotoImage(img)
			charac = Label(root, image=render, bg='#222B36', bd=0)
			charac.image = render
			charac.place(x=310, y=50)
			plt.clf()
		except ValueError:
			tkMessageBox.showerror("Error", "Consulte el apartado de Ayuda para más información sobre este error.")

	calcBtn = Button(root, text="Calcular y graficar", command=calcularF).place(x=120, y=270)

def polLagAct():
	fondo()
	L1 = Label(root, bg='#272822', font=("Helvetica", 16), fg='#5591C7', justify=LEFT, text="Polinomio de interpolación Lagrange").place(x=10, y=10)
	L2 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="x = ").place(x=30, y=50)
	xString = StringVar()
	xEntry = Entry(root, bd=1, width=20, textvariable=xString).place(x=70, y=50)
	L4 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="y = ").place(x=30, y=80)
	yString = StringVar()
	y3Entry = Entry(root, bd=1, width=20, textvariable=yString).place(x=70, y=80)

	img = Image.open('img\output.png')
	img = img.resize((600, 400), Image.ANTIALIAS)
	render = ImageTk.PhotoImage(img)
	charac = Label(root, image=render, bg='#222B36', bd=0)
	charac.image = render
	charac.place(x=310, y=50)

	def calcularF():
		try:
			xtext = xString.get()
			ytext = yString.get()
			xArray = eval('[' + xtext + ']')
			yArray = eval('[' + ytext + ']')
			
			if len(xArray) == len(yArray):
				x = map(float, xArray)
				y = map(float, yArray)
				n=int(len(xArray))

				result = scipy.poly1d([0.0])
				for i in range(0, n):
					temp_numerator = scipy.poly1d([1.0])
					denumerator = 1.0
					for j in range(0, n):
						if i != j:
							temp_numerator *= scipy.poly1d([1.0, -x[j]])
							denumerator *= x[i] - x[j]
					result += (temp_numerator / denumerator) * y[i]

				x_val = np.arange(min(x), max(x) + 1, 0.1)
				plt.plot(x_val, result(x_val))
				plt.axis([min(x) - 1, max(x) + 1, min(y) - 1, max(y) + 1])
				pyplot.axhline(0, color="black")
				pyplot.axvline(0, color="black")
				for i in range(0, len(x)):
					plt.plot([x[i]], [y[i]], 'bo')
				plt.ylim(-100, 100)
				plt.xlim(-100, 100)
				plt.savefig("img\dlagrange.png")

				cosa =  repr(result)
				L0 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, width=30, height=5, text="Resultado = \n" + cosa).place(x=30, y=300)

				img = Image.open("img\dlagrange.png")
				img = img.resize((600, 400), Image.ANTIALIAS)
				render = ImageTk.PhotoImage(img)
				charac = Label(root, image=render, bg='#222B36', bd=0)
				charac.image = render
				charac.place(x=310, y=50)
				plt.clf()

			else:
				tkMessageBox.showerror("Error", "'x' y 'y' deben contener la misma cantidad de caracteres")
		except ValueError:
			tkMessageBox.showerror("Error", "Consulte el apartado de Ayuda para más información sobre este error.")

	calcBtn = Button(root, text="Calcular y graficar", command=calcularF).place(x=70, y=110)

def newRapAct():
	fondo()
	L1 = Label(root, bg='#272822', font=("Helvetica", 16), fg='#5591C7', justify=LEFT, text="Newton- Raphson").place(x=10, y=10)
	L2 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="x0 = ").place(x=30, y=50)
	x0String = StringVar()
	x0Entry = Entry(root, bd=1, width=20, textvariable=x0String).place(x=120, y=50)
	L4 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="X³ = ").place(x=30, y=80)
	X3String = StringVar()
	x3Entry = Entry(root, bd=1, width=20, textvariable=X3String).place(x=120, y=80)
	L5 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="X² = ").place(x=30, y=110)
	X2String = StringVar()
	x2Entry = Entry(root, bd=1, width=20, textvariable=X2String).place(x=120, y=110)
	L7 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="Const = ").place(x=30, y=140)
	CString = StringVar()
	CEntry = Entry(root, bd=1, width=20, textvariable=CString).place(x=120, y=140)

	img = Image.open('img\output.png')
	img = img.resize((600, 400), Image.ANTIALIAS)
	render = ImageTk.PhotoImage(img)
	charac = Label(root, image=render, bg='#222B36', bd=0)
	charac.image = render
	charac.place(x=310, y=50)

	def f(x):
    		return x3*(x**3) + x2*(x**2) + const

	def calcularF():
		try:
			global x0, x3, x2, const
			x0 = float(x0String.get())
			x3 = float(X3String.get())
			x2 = float(X2String.get())
			const = float(CString.get())

			scipy.optimize.newton(f, x0)
			raiz = repr(scipy.optimize.newton(f, x0))

			L0 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, width=30, height=5, text="Raíz = " + raiz).place(x=30, y=300)

			x = np.linspace(-10,10)
			plt.plot(x, f(x));
			plt.axhline(0, color="black")
			plt.axvline(0, color="black")
			plt.ylim(-100, 100)
			plt.xlim(-100, 100)
			plt.savefig("img\dnewrap.png")

			img = Image.open("img\dnewrap.png")
			img = img.resize((600, 400), Image.ANTIALIAS)
			render = ImageTk.PhotoImage(img)
			charac = Label(root, image=render, bg='#222B36', bd=0)
			charac.image = render
			charac.place(x=310, y=50)
			plt.clf()
		except ValueError:
			tkMessageBox.showerror("Error", "Consulte el apartado de Ayuda para más información sobre este error.")

	calcBtn = Button(root, text="Calcular y graficar", command=calcularF).place(x=120, y=170)

def DDNAct():
	fondo()
	L1 = Label(root, bg='#272822', font=("Helvetica", 16), fg='#5591C7', justify=LEFT, text="Diferencias divididas con Newton").place(x=10, y=10)
	L2 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="x = ").place(x=30, y=50)
	xString = StringVar()
	xEntry = Entry(root, bd=1, width=20, textvariable=xString).place(x=100, y=50)
	L3 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="y = ").place(x=30, y=80)
	yString = StringVar()
	yEntry = Entry(root, bd=1, width=20, textvariable=yString).place(x=100, y=80)
	L4 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="k [f(k)] = ").place(x=30, y=110)
	kString = StringVar()
	kEntry = Entry(root, bd=1, width=20, textvariable=kString).place(x=100, y=110)

	def calcularF():
		try:

			xtext = xString.get()
			ytext = yString.get()
			k = kString.get()
			x = eval('[' + xtext + ']')
			y = eval('[' + ytext + ']')
			f1 = 1
			f2 = 0
			j = 1
			p = [len(x)]
			n = len(x)

			if len(x) == len(y):
				f=y[1]
				while True:
					for i in range(1, len(x)-1):
						#p[i] = ((y[i+1]-y[i])/(x[i+j]-x[i]))
						c = ((y[i+1]-y[i])/(x[i+j]-x[i]))
						p.append(c)
						y[i]=p[i]
					f1=1
					for i in range(1, j):
						f1*=(k-x[i])

					f2+=(y[1]*f1)
					n=n-1
					j=j+1
					if n!=1:
						break
				f+=f2

				L0 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, width=30, height=5, text="f(" + repr(k) + ") = " + repr(f)).place(x=30, y=300)
			else:
				tkMessageBox.showerror("Error", "'x' y 'y' deben contener la misma cantidad de caracteres")
		except ValueError:
			tkMessageBox.showerror("Error", "Consulte el apartado de Ayuda para más información sobre este error.")

	calcBtn = Button(root, text="Calcular", command=calcularF).place(x=100, y=140)


def APMCAct():
	fondo()
	L1 = Label(root, bg='#272822', font=("Helvetica", 16), fg='#5591C7', justify=LEFT, text="Aproximación polinomial con mínimos cuadrados").place(x=10, y=10)
	L2 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="Puntos (x)= ").place(x=30, y=50)
	xString = StringVar()
	nEntry = Entry(root, bd=1, width=20, textvariable=xString).place(x=170, y=50)
	L3 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="Pendiente = ").place(x=30, y=80)
	penString = StringVar()
	penEntry = Entry(root, bd=1, width=20, textvariable=penString).place(x=170, y=80)

	img = Image.open('img\output.png')
	img = img.resize((600, 400), Image.ANTIALIAS)
	render = ImageTk.PhotoImage(img)
	charac = Label(root, image=render, bg='#222B36', bd=0)
	charac.image = render
	charac.place(x=310, y=50)

	def calcularF():
		try:
			aux = xString.get()
			puntos = eval('[' + aux + ']')
			n = len(puntos)
			#n = int(nString.get())
			#random.seed(n)
			#x = linspace(0.0,1.0,n)
			x = np.array(puntos)
			noiz = random.normal(0,0.25,n)
			me = float(penString.get())
			be = 3.0
			ye = me*x+be + noiz
			plt.plot(x,ye,'o')
			A = array([x,zeros(n)+1])
			A = A.transpose();
			result = linalg.lstsq(A,ye)
			m,b = result[0] 			##pendiente m; intercepto b
			
			L0 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, width=30, height=5, text="m = " + repr(m) + "\nb = " + repr(b)).place(x=30, y=300)

			plt.hold(True)
			plt.plot(x,m*x+b)
			plt.axhline(0, color="black")
			plt.axvline(0, color="black")
			plt.savefig("img\APMC.png")

			img = Image.open("img\APMC.png")
			img = img.resize((600, 400), Image.ANTIALIAS)
			render = ImageTk.PhotoImage(img)
			charac = Label(root, image=render, bg='#222B36', bd=0)
			charac.image = render
			charac.place(x=310, y=50)
			plt.clf()
		except ValueError:
			tkMessageBox.showerror("Error", "Consulte el apartado de Ayuda para más información sobre este error.")

	calcBtn = Button(root, text="Graficar", command=calcularF).place(x=170, y=110)


def DFNAct():
	fondo()
	L1 = Label(root, bg='#272822', font=("Helvetica", 16), fg='#5591C7', justify=LEFT, text="Diferencias finitas con Newton").place(x=10, y=10)
	L2 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="Vector inicial (x) = ").place(x=30, y=50)
	viString = StringVar()
	viEntry = Entry(root, bd=1, width=20, textvariable=viString).place(x=170, y=50)
	L3 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="Vector final (y) = ").place(x=30, y=80)
	vfString = StringVar()
	vfEntry = Entry(root, bd=1, width=20, textvariable=vfString).place(x=170, y=80)
	L4 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="Tamaño (h) = ").place(x=30, y=110)
	tString = StringVar()
	tEntry = Entry(root, bd=1, width=20, textvariable=tString).place(x=170, y=110)

	img = Image.open('img\output.png')
	img = img.resize((600, 400), Image.ANTIALIAS)
	render = ImageTk.PhotoImage(img)
	charac = Label(root, image=render, bg='#222B36', bd=0)
	charac.image = render
	charac.place(x=310, y=50)
	def calcularF():
		try:
			h = float(tString.get())
			vi = float(viString.get())
			vf = float(vfString.get())
			x = np.arange(vi, vf+h, h)
			y = np.empty(len(x))
			y[0] = 1
			for i in range(0,len(x) - 1):
				#y[i+1] = h*np.exp(x[i]) + (h*x[i] + 1)*y[i]
				y[i+1] = y[i]/(1 - 5*h)
			p.plot(x, y, label="Aproximacion")
			p.legend(loc="best")
			p.axhline(0, color="black")
			p.axvline(0, color="black")
			p.savefig("img\DFN.png")

			img = Image.open("img\DFN.png")
			img = img.resize((600, 400), Image.ANTIALIAS)
			render = ImageTk.PhotoImage(img)
			charac = Label(root, image=render, bg='#222B36', bd=0)
			charac.image = render
			charac.place(x=310, y=50)
			plt.clf()
		except ValueError:
			tkMessageBox.showerror("Error", "Consulte el apartado de Ayuda para más información sobre este error.")
		
	calcBtn = Button(root, text="Graficar", command=calcularF).place(x=170, y=140)


def posFalsa():
	fondo()
	L1 = Label(root, bg='#272822', font=("Helvetica", 16), fg='#5591C7', justify=LEFT, text="Posición falsa").place(x=10, y=10)
	L2 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="La regla falsa es un método iterativo de resolución numérica de ecuaciones no lineales.\nEl método combina el método de bisección y el método de la secante.\n"+
		"Como en el método de bisección, se parte de un intervalo inicial [a0,b0] con f(a0) y f(b0) de signos opuestos, \nlo que garantiza que en su interior hay al menos una raíz. El algoritmo va obteniendo sucesivamente en cada\npaso un intervalo más pequeño [ak, bk] que sigue incluyendo una raíz de la función f.\n"+
		"A partir de un intervalo [ak, bk] se calcula un punto interior ck:").place(x=10, y=50)
	load = Image.open('img\img1.png')
	render = ImageTk.PhotoImage(load)
	charac = Label(root, image=render, bg='#222B36', bd=0)
	charac.image = render
	charac.place(x=30, y=190)
	L3 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="Dicho punto es la intersección de la recta que pasa por (a,f(ak)) y (b,f(bk)) con el eje de abscisas (igual a como\nse hace en el método de la secante)."
		"Se evalúa entonces f(ck). Si es suficientemente pequeño, ck es la raíz buscada.\nSi no, el próximo intervalo [ak+1, bk+1] será:\n"
		"[ak, ck] si f(ak) y f(ck) tienen signos opuestos;\n"
		"[ck, bk] en caso contrario.").place(x=10, y=280)

def encRaiz():
	fondo()
	L1 = Label(root, bg='#272822', font=("Helvetica", 16), fg='#5591C7', justify=LEFT, text="Encontrar raiz").place(x=10, y=10)
	L2 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="Una ecuación cuadrática de una variable es una ecuación que tiene la forma de una suma algebraica de términos\ncuyo grado máximo es dos, es decir, una ecuación cuadrática puede ser representada por un polinomio de segundo grado\no polinomio cuadrático. La expresión canónica general de una ecuación cuadrática de una variable es:\n\n"+
		"ax² + bx + c = 0, donde a ≠ 0 \n\ndonde x es la variable, y a, b y c constantes;").place(x=10, y=50)
	load = Image.open('img\img2.png')
	render = ImageTk.PhotoImage(load)
	charac = Label(root, image=render, bg='#222B36', bd=0)
	charac.image = render
	charac.place(x=30, y=200)

def apPolSim():
	fondo()
	L1 = Label(root, bg='#272822', font=("Helvetica", 16), fg='#5591C7', justify=LEFT, text="Aproximación polinomial simple").place(x=10, y=10)
	L2 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="La interpolación polinomial es una técnica de interpolación de un conjunto de datos o de una función por un polinomio.\nEs decir, dado cierto número de puntos obtenidos por muestreo o a partir de un experimento se pretende encontrar un\npolinomio que pase por todos los puntos."+
		"\nSea Fn una variable discreta de n elementos y sea Xn otra variable discreta de n  elementos los cuales\ncorresponden, por parejas, a la imagen u ordenada y abcisa de los datos que se quieran interpolar, respectivamente,\ntales que:").place(x=10, y=50)
	load = Image.open('img\img3.png')
	render = ImageTk.PhotoImage(load)
	charac = Label(root, image=render, bg='#222B36', bd=0)
	charac.image = render
	charac.place(x=30, y=200)

def bisec():
	fondo()
	L1 = Label(root, bg='#272822', font=("Helvetica", 16), fg='#5591C7', justify=LEFT, text="Bisección").place(x=10, y=10)
	L2 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="El método de bisección es un algoritmo de búsqueda de raíces que trabaja dividiendo el intervalo a la mitad y\nseleccionando el subintervalo que tiene la raíz."+
		"\nPrimero se calcula el punto medio del intervalo:").place(x=10, y=50)
	load = Image.open('img\img4.png')
	render = ImageTk.PhotoImage(load)
	charac = Label(root, image=render, bg='#222B36', bd=0)
	charac.image = render
	charac.place(x=30, y=130)
	L2 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="después se averigua sí f(a)f(c)<0. Si lo es, entonces f tiene un cero en [a,c].\n"+
		"A continuación se renombra a c como b y se comienza una vez más con el nuevo intervalo [a,b], cuya longitud es\nigual a la mitad del intervalo original."+
		"\nSi f(a)f(c)>0 , entonces f(c)f(b)<0 y en este caso se renombra a c como a."+
		"\nEn ambos casos se ha generado un nuevo intervalo que contiene un cero de f, y el proceso puede repetirse.").place(x=10, y=200)

def polLag():
	fondo()
	L1 = Label(root, bg='#272822', font=("Helvetica", 16), fg='#5591C7', justify=LEFT, text="Polinomio de interpolación Lagrange").place(x=10, y=10)
	L2 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="Es una forma de presentar el polinomio que interpola un conjunto de puntos dado.\n"+
		"Empezamos con un conjunto de n+1 puntos en el plano (que tengan diferentes coordenadas x):\n"+
		"(x0, y0), (x1, y1), (x2, y2),....,(xn, yn).\n"+
		"Nuestro objetivo es encontrar una función polinómica que pase por esos n+1 puntos y que tengan el menor grado posible.\nUn polinomio que pase por varios puntos determinados se llama un polinomio de interpolación."+
		"\nLa fórmula general para el polinomio de interpolación de Lagrange es:").place(x=10, y=50)
	load = Image.open('img\img5.png')
	render = ImageTk.PhotoImage(load)
	charac = Label(root, image=render, bg='#222B36', bd=0)
	charac.image = render
	charac.place(x=30, y=190)

def newRap():
	fondo()
	L1 = Label(root, bg='#272822', font=("Helvetica", 16), fg='#5591C7', justify=LEFT, text="Newton - Raphson").place(x=10, y=10)
	L2 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="El método Newton - Raphson es un algoritmo para encontrar aproximaciones de los ceros o raíces de una función real.\nTambién puede ser usado para encontrar el máximo o mínimo de una función, encontrando los ceros de su primera derivada.\n"+
		"El método de Newton-Raphson es un método abierto, en el sentido de que no está garantizada su convergencia global.\nLa única manera de alcanzar la convergencia es seleccionar un valor inicial lo suficientemente cercano a la raíz buscada.\n"+
		"Se realizarán sucesivas iteraciones hasta que el método haya convergido lo suficiente.\n"+
		"Sea f: [a, b] -> R función derivable definida en el intervalo real [a, b]. \nEmpezamos con un valor inicial x0 y definimos para cada número natural n").place(x=10, y=50)
	load = Image.open('img\img6.png')
	render = ImageTk.PhotoImage(load)
	charac = Label(root, image=render, bg='#222B36', bd=0)
	charac.image = render
	charac.place(x=30, y=200)

def divNew():
	fondo()
	L1 = Label(root, bg='#272822', font=("Helvetica", 16), fg='#5591C7', justify=LEFT, text="Diferencias divididas con Newton").place(x=10, y=10)
	L2 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="Es un método de interpolación polinómica. Aunque sólo existe un único polinomio que interpola una serie de puntos,\nexisten diferentes formas de calcularlo. Este método es útil para situaciones que requieran un número bajo de puntos\npara interpolar, ya que a medida que crece el número de puntos, también lo hace el grado del polinomio."+
		"\nLa forma general del polinomio interpolante de Newton para n+1 datos (x0, ƒ(x0)), (x1, ƒ(x1)), ..., (xn, ƒ(xn)) es:").place(x=10, y=50)
	load = Image.open('img\img7.png')
	render = ImageTk.PhotoImage(load)
	charac = Label(root, image=render, bg='#222B36', bd=0)
	charac.image = render
	charac.place(x=30, y=160)
	L3 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="Los coeficientes ai se obtienen calculando un conjunto de cantidades denominadas diferencias divididas.").place(x=10, y=220)

def ApPolMinCua():
	fondo()
	L1 = Label(root, bg='#272822', font=("Helvetica", 16), fg='#5591C7', justify=LEFT, text="Aproximación polinomial con mínimos cuadrados").place(x=10, y=10)
	L2 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="La mejor aproximación deberá tender a interpolar la función de la que proviene el conjunto de pares (Xk, Yk), esto es,\ndeberá tender a pasar exactamente por todos los puntos. Eso supone que se debería cumplir que:").place(x=10, y=50)
	load = Image.open('img\img8.png')
	render = ImageTk.PhotoImage(load)
	charac = Label(root, image=render, bg='#222B36', bd=0)
	charac.image = render
	charac.place(x=30, y=100)
	L3 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="Sustituyendo f(x) por su expresión como combinación lineal de una base de m funciones:").place(x=10, y=150)
	load2 = Image.open('img\img9.png')
	render2 = ImageTk.PhotoImage(load2)
	charac2 = Label(root, image=render2, bg='#222B36', bd=0)
	charac2.image = render2
	charac2.place(x=30, y=200)

def difFinNew():
	fondo()
	L1 = Label(root, bg='#272822', font=("Helvetica", 16), fg='#5591C7', justify=LEFT, text="Diferencias finitas con Newton").place(x=10, y=10)
	L2 = Label(root, bg='#272822', font=("Helvetica", 12), fg='#FFF', justify=LEFT, text="Una diferencia finita es una expresión matemática de la forma f(x + b) − f(x +a).\n"+
		"En el caso particular de que las abcisas de los nodos de interpolación sean equidistantes la expresión del polinomio\nde interpolación de Newton en diferencias divididas adopta otras formas que se han usado mucho, la fórmula en\ndiferencias progresivas y la fórmula en diferencias regresivas.").place(x=10, y=50)
	load = Image.open('img\img10.png')
	render = ImageTk.PhotoImage(load)
	charac = Label(root, image=render, bg='#222B36', bd=0)
	charac.image = render
	charac.place(x=30, y=150)


def winAyuda():
	r = Tk()
	r.title('Ayuda')
	r.iconbitmap('img\icon.ico')
	r.geometry('320x400')
	r.config(bg='#272822')
	r.resizable(0,0)

	left = Frame(r)
	right = Frame(r)
	t_start = Text(left, width=30, fg="#FFF", bg="#272822", bd=0, font=("Arial", 12), padx=20, pady=20)
	t_start.insert(END, "• Posición falsa:\nEn los apartado x0 y x1 deberá ingresar los datos a calcular. Este arrojará la raíz corresponiente.\n\n"+
		"• Encontrar la raiz:\nSólo se podrán ingresar números positivos, negativos, enteros o con punto flotante.\n'a' debe ser diferente de 0, y para poder dar las raices y graficar, el resultado no debe ser un número imaginario.\n\n"+
		"• Aproximación polinomial simple:\nEn este apartado el usuario sólo ingresará los datos de la ecuación ya establecida. Siguiendo las normas anteriores para evitar errores.\n\n"+
		"• Bisección:\nPara poder dar un resultado y graficarlo es necesario que f(a) y f(b) deban tener signos diferentes.\n\n"+
		"• Polinomio de interpolación Lagrange:\nEn este apartado, si se quiere obtener mas de un punto pude ingresar varíos números separándolos con una coma ','. Ejemplo: x: 1,2,3; y: -9,12,5.\n\n"+
		"• Newton-Raphson:\nSólo se podrán ingresar números positivos, negativos, enteros o con punto flotante. Si se excede el número de iteraciones a causa de los datos ingresados, este no dará ningún resultado.\n\n"+
		"• Diferencias divididas con Newton:\nEn este apartado, si se quiere obtener mas de un punto pude ingresar varíos números separándolos con una coma ','.\n\n"+
		"• Aproximación polinomial con mínimos cuadrados\nUnicamente se podrá ingresar un número entero y positivo en el apartado 'No. de puntos'. En el apartado 'pendiente' se podrá ingresar un número, ya sea con punto flotante, entero, positivo o negativo.\n\n"+
		"• Diferencias finitas con Newton\nEn los apartado de 'x', 'y' y 'h' se podrán ingresar números enteros, fotantes, positivos o negativos.\n\n"+
		"En caso de presentar algún problema reinicie el software.\nSi el problema persiste, presenta algún dato incorrecto o tiene alguna duda, mande correo a:\n\nalvaro.rodriguez.sc@itszapopan.edu.mx\n\n")
	t_start.pack(side=LEFT, fill=Y)
	s_start = Scrollbar(left)
	s_start.pack(side=RIGHT, fill=Y)
	s_start.config(command=t_start.yview)
	t_start.config(yscrollcommand=s_start.set, state=DISABLED) #, state=DISABLED
	left.pack(side=LEFT, fill=Y)
	right.pack(side=RIGHT, fill=Y)

	#r.mainloop()

def winInfo():
	tkMessageBox.showinfo("Información de software", "Software desarrollado para la asignatura de métodos numéricos.\n\nDesarrollado por:\tAzael Rodríguez\n\nVersión de software:\t1.0")

##creación de menús
menubar = Menu(root)

##Cascada de info
mnuInfo = Menu(menubar, tearoff=0)
subInfo1 = Menu(menubar, tearoff=0)
subInfo2 = Menu(menubar, tearoff=0)
subInfo3 = Menu(menubar, tearoff=0)
mnuInfo.add_cascade(label="Parcial 1", menu=subInfo1)
subInfo1.add_command(label="Posición falsa", command=posFalsa)
subInfo1.add_command(label="Encontrar la raiz", command=encRaiz)
mnuInfo.add_cascade(label="Parcial 2", menu=subInfo2)
subInfo2.add_command(label="Aproximación polinomial simple", command=apPolSim)
subInfo2.add_command(label="Bisección", command=bisec)
subInfo2.add_command(label="Polinomio de interpolación Lagrange", command=polLag)
subInfo2.add_command(label="Newton-Raphson", command=newRap)
mnuInfo.add_cascade(label="Parcial 3", menu=subInfo3)
subInfo3.add_command(label="Diferencias divididas con Newton", command=divNew)
subInfo3.add_command(label="Aproximación polinomial con mínimos cuadrados", command=ApPolMinCua)
subInfo3.add_command(label="Diferencias finitas con Newton", command=difFinNew)
mnuInfo.add_separator()
mnuInfo.add_command(label="Limpiar ventana", command=fondo)
mnuInfo.add_separator()
mnuInfo.add_command(label="Salir", command=root.quit)
menubar.add_cascade(label="Archivo", menu=mnuInfo)

##Parcial 1
menuPar1 = Menu(menubar, tearoff=0)
menuPar1.add_command(label="Posición falsa", command=posFalsaAct)
menuPar1.add_command(label="Encontrar la raiz", command=encRaizAct)
menubar.add_cascade(label="Parcial 1", menu=menuPar1)

##Parcial 2
menuPar2 = Menu(menubar, tearoff=0)
menuPar2.add_command(label="Aproximación polinomial simple", command=apPolSimAct)
menuPar2.add_command(label="Bisección", command=bisecAct)
menuPar2.add_command(label="Polinomio de interpolación Lagrange", command=polLagAct)
menuPar2.add_command(label="Newton-Raphson", command=newRapAct)
menubar.add_cascade(label="Parcial 2", menu=menuPar2)

#Parcial 3
menuPar3 = Menu(menubar, tearoff=0)
menuPar3.add_command(label="Diferencias divididas con Newton", command=DDNAct)
menuPar3.add_command(label="Aproximación polinomial con mínimos cuadrados", command=APMCAct)
menuPar3.add_command(label="Diferencias finitas con Newton", command=DFNAct)
menubar.add_cascade(label="Parcial 3", menu=menuPar3)

##Ayuda
helpmenu = Menu(menubar, tearoff=0)
helpmenu.add_command(label="Ver ayuda", command=winAyuda)
helpmenu.add_separator()
helpmenu.add_command(label="Información de software", command=winInfo)
menubar.add_cascade(label="Ayuda", menu=helpmenu)

# display the menu
root.config(menu=menubar)
root.mainloop()